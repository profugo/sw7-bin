# SpaceWar7

Juego de estrategia por turnos

# Empezando

Estos son los fuentes en qBasic de un juego del año del moco remozado para que sea jugable hoy. Los fuentes están muy documentados para que un programador novato se haga una idea de que está pasando.
Si sólo querés jugar al juego, lo encontrás en [gitlab.com](https://gitlab.com/profugo/sw7-bin), hay versión para Linux y para windows.

## Instalación
¿Instalación? Este juego no precisa instalación, descomprimilo donde quieras y usalo. Si querés ver los fuentes pegate una vuelta por [gitlab.com](https://gitlab.com/profugo/sw7) y bajalos.

## Uso
Lo extraés y corrés el ejecutable, sw7 para el juego versión 2022 o sw7_orig para la versión ochentosa.

## Soporte
Un valiente no precisa ayuda, es decir, no hay

## Licencia
Este proyecto fué liberado bajo licencia [GNU GPL](https://www.gnu.org/licenses/licenses.es.html#GPL), bueh, lo que yo hice al menos, el resto:

### Sonidos
* over.ogg: [GFX Sounds](https://www.youtube.com/watch?v=9IcGDIk4i8s)
* Música: [FMA](https://freemusicarchive.org/music/Damiano_Baldoni/Old_Beat/unreachable)
* Las otras voces las hice yo

### Gráficos
* nave.png,  krinng.png: [Open Game Art](https://opengameart.org/content/top-down-space-ships)
* Imágen de portada: [Art Station](https://www.artstation.com/artwork/0nOewE)
* Los otros gráficos son invento mio

### Fuentes:
* Airbore GP: [1001 Fonts](https://www.1001fonts.com/airborne-gp-font.html)
* Orbitron Medium: [Dafont Free](https://www.dafontfree.net/orbitron-medium/f92274.htm)

## Estado del proyecto
Terminando
